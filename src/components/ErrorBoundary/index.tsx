import React, { Component } from 'react';

import styles from './style.module.less';

// 定义Props接口
interface ErrorBoundaryProps {
  children: React.ReactNode;
}

// 定义State接口
interface ErrorBoundaryState {
  hasError: boolean;
  errorMessage?: string;
  errorInfo?: React.ErrorInfo;
}

class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(_error: Error): Partial<ErrorBoundaryState> {
    // 更新state以标记有错误发生
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    // 在此处记录错误到服务器或日志服务
    console.error('Uncaught error:', error, errorInfo);

    // 保存错误信息到state，以便在渲染阶段显示给用户
    this.setState({
      errorMessage: error.toString(),
      errorInfo,
    });
  }

  render() {
    if (this.state.hasError) {
      // 渲染错误恢复界面
      return (
        <div className={styles.errorBoundary}>
          <h1>加载出错，请排查错误并刷新页面</h1>
          <p>{this.state.errorMessage}</p>
          {process.env.NODE_ENV === 'development' && (
            <details style={{ whiteSpace: 'pre-wrap' }}>
              {this.state.errorInfo?.componentStack}
            </details>
          )}
        </div>
      );
    }

    // 当没有错误时，正常渲染子组件
    return this.props.children;
  }
}

export default ErrorBoundary;
