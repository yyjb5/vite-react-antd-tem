$PSDefaultParameterValues['*:Encoding'] = 'utf8'
# chcp 65001
$outputLines = code --list-extensions | ForEach-Object { $_ }
$patternsToFind = @("dbaeumer.vscode-eslint","esbenp.prettier-vscode",  "stylelint.vscode-stylelint")

$foundAllPatterns = $true
foreach ($pattern in $patternsToFind) {
    if (-not ($outputLines -contains $pattern)) {  
        Write-Output $pattern
        $foundAllPatterns = $false
    }
}

if (-not $foundAllPatterns) {
    Write-Output "插件未安装，建议vscode安装"
} 
else {
    Write-Output "所有建议插件都已经安装"
}