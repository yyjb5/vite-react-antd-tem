const { exec } = require('child_process');

// 假设要执行的命令是 'ls'，并在其中查找是否存在名为 "file1.txt" 和 "file2.txt" 的文件
const command = 'code --list-extensions';
const searchLines = [
  'dbaeumer.vscode-eslint',
  'esbenp.prettier-vscode',
  'stylelint.vscode-stylelint',
];
let missingItems = [];

// 创建子进程
exec(command, (error, stdout, stderr) => {
  if (error) {
    console.error(`执行命令时出错: ${error}`);
    console.error(stderr);
    return;
  }

  // 按行分割输出并存储到数组中
  const outputLines = stdout.trim().split('\n');

  for (let item of searchLines) {
    if (!outputLines.includes(item)) {
      missingItems.push(item);
    }
  }
  if (missingItems.length === 0) {
    console.log('所有建议插件都已经安装');
  } else {
    console.log('以下插件未安装，建议vscode安装:', missingItems);
  }
});
